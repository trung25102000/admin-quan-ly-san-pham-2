﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _19T1021288.DomainModels
{
    /// <summary>
    /// Nhân viên
    /// </summary>
    public class Employee
    {
        /// <summary>
        /// Mã khách hàng
        /// </summary>
        public int EmployeeID { get; set; }

        /// <summary>
        /// Tên
        /// </summary>
        public String LastName { get; set; }

        /// <summary>
        /// Họ lót và chữ đệm
        /// </summary>
        public String FirstName { get; set; }

        /// <summary>
        /// Ngày sinh nhật
        /// </summary>
        public DateTime BirthDate { get; set; }

        /// <summary>
        /// Đường link ảnh
        /// </summary>
        public String Photo { get; set; }

        /// <summary>
        /// Ghi chú
        /// </summary>
        public string Notes { get; set; }

        /// <summary>
        /// Email
        /// </summary>
        public String Email { get; set; }

        /// <summary>
        /// Mật khẩus
        /// </summary>
        public String Password { get; set; }
    }
}

