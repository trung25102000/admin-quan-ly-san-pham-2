﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _19T1021288.DomainModels
{
    /// <summary>
    /// Tài khoản người dùng
    /// </summary>
    public class UserAcount
    {
        /// <summary>
        /// Mã tài khoản.
        /// </summary>
        public string UserID { get; set; }

        /// <summary>
        /// Tên người dùng
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// Tên đầy đủ ng dùng
        /// </summary>
        public string FullName { get; set; }

        /// <summary>
        /// Email
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Mật khẩu
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// Nhóm tên
        /// </summary>
        public string RoleNames { get; set; }

        /// <summary>
        /// Ảnh.
        /// </summary>
        public string Images { get; set; }

        /// <summary>
        /// ngày sinh.
        /// </summary>
        public DateTime Birthdate;
    }
}
