﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _19T1021288.DomainModels
{
    /// <summary>
    /// Loại hàng
    /// </summary>
    public class Category
    {
        /// <summary>
        /// Mã loại hàng
        /// </summary>
        public int CategoryID { get; set; }

        /// <summary>
        /// Tên loại hàng
        /// </summary>
        public String CategoryName { get; set; }

        /// <summary>
        /// Ghi chú
        /// </summary>
        public String Description { get; set; }

        /// <summary>
        /// Mã danh mục gốc
        /// </summary>
        public int ParentCategoryId { get; set; }

       
    }
}
