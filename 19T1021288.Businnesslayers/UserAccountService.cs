﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using _19T1021288.DomainModels;
using _19T1021288.DataLeyers;

namespace _19T1021288.Businnesslayers
{
    /// <summary>
    /// Các chức năng nghiệp vụ liên quan đến tài khoản.
    /// </summary>
    public static class UserAccountService
    {
        private static readonly IUserAccountDAL EmployeeAccountDB;
        private static readonly IUserAccountDAL CustomerAccountDB;

        /// <summary>
        /// Hàm tạo
        /// </summary>
        static UserAccountService()
        {
            String conectionString = ConfigurationManager.ConnectionStrings["DB"].ConnectionString;

            EmployeeAccountDB = new DataLeyers.SQLServer.EmployeeAccountDAL(conectionString);

            CustomerAccountDB = new DataLeyers.SQLServer.CustomerAccountDAL(conectionString);
        }

        #region Nghiệp vụ liên quan đến tài khoản của nhân viên và khách hàng.

        /// <summary>
        /// Kiểm tra tính hợp lệ.
        /// </summary>
        /// <param name="accountType"></param>
        /// <param name="userName"></param>
        /// <param name="Password"></param>
        /// <returns></returns>
        public static UserAcount Authorize(AccountTypes accountType,string userName,string Password)
        {
            if(accountType==AccountTypes.Employee)
            {
                return EmployeeAccountDB.Authorize(userName, Password);
            }
            else
            {
                return CustomerAccountDB.Authorize(userName, Password);
            }    
        }


        /// <summary>
        /// Đổi mật khẩu tùy vào loại tài khoản.
        /// </summary>
        /// <param name="accountType"></param>
        /// <param name="username"></param>
        /// <param name="OldPassword"></param>
        /// <param name="NewPassword"></param>
        /// <returns></returns>
        public static bool ChangePassword(AccountTypes accountType,string username,String OldPassword,String NewPassword)
        {
            if(accountType==AccountTypes.Employee)
            {
                return EmployeeAccountDB.ChangePasswword(username, OldPassword, NewPassword);
            }
            else
            {
                return CustomerAccountDB.ChangePasswword(username, OldPassword, NewPassword);
            }    
        }
        #endregion
    }
}
