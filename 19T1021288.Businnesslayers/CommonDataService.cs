﻿
using _19T1021288.DomainModels;
using _19T1021288.DataLeyers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

namespace _19T1021288.Businnesslayers
{
    /// <summary>
    /// Các chức năng xử lý nghiệp vụ liên quan đến nhà cung cấp,
    /// khách hàng,nhân viên,người giao hàng,loại hàng
    /// </summary>
    public static class CommonDataService
    {
        private static readonly ICountryDAL countryDB;
        private static readonly ICommonDAL<Supplier> SupplierDB;
        private static readonly ICommonDAL<Shipper> ShipperDB;
        private static readonly ICommonDAL<Category> CategoryDB;
        private static readonly ICommonDAL<Customer> CustomerDB;
        private static readonly ICommonDAL<Employee> EmployeeDB;

        /// <summary>
        /// Ctors
        /// </summary>
        static CommonDataService()
        {
            String conectionString = ConfigurationManager.ConnectionStrings["DB"].ConnectionString;

            countryDB = new DataLeyers.SQLServer.CountryDAL(conectionString);
            SupplierDB = new DataLeyers.SQLServer.SupplierDAL(conectionString);
            ShipperDB = new DataLeyers.SQLServer.ShipperDAL(conectionString);
            CategoryDB = new DataLeyers.SQLServer.CategoryDAL(conectionString);
            CustomerDB = new DataLeyers.SQLServer.CustomerDAL(conectionString);
            EmployeeDB = new DataLeyers.SQLServer.EmployeeDAL(conectionString);

        }

        #region Các nghiệp vụ liên quan đến quốc gia
        /// <summary>
        /// Lấy danh sách các quốc gia
        /// </summary>
        /// <returns></returns>
        public static List<Country> ListOfCountries()
        {
            return countryDB.List().ToList();
        }
        #endregion
        #region Các nghiệp vụ liên quan đến nhà cung cấp

        /// <summary>
        /// Tìm kiếm, Lấy danh sách các nhà cung cấp dưới dạng phân trang.
        /// </summary>
        /// <param name="Page">Trang cần xem</param>
        /// <param name="PageSize"> Số dòng trên mối trang(=0 nếu không phân trang)</param>
        /// <param name="SearchValue">Giá trị tìm kiếm (=rỗng nếu không tìm kiếm)</param>
        /// <param name="RowCount">Output: Tổng số dòng tìm được</param>
        /// <returns></returns>
        public static List<Supplier> ListOfSuppliers(int Page, int PageSize, String SearchValue, out int RowCount)
        {
            RowCount = SupplierDB.Count(SearchValue);
            return SupplierDB.List(Page, PageSize, SearchValue).ToList();
        }


        /// <summary>
        /// tìm kiếm và lấy danh sách nhà cung cấp nếu ko phân trang.
        /// </summary>
        /// <param name="SearchValue"></param>
        /// <returns></returns>
        public static List<Supplier> ListOfSuppliers(String SearchValue)
        {
            return SupplierDB.List(1, 0, SearchValue).ToList();
        }

        /// <summary>
        /// Bổ sung 1 nhà cung cấp
        /// </summary>
        /// <param name="Data">Dữ liệu nhà cung cấp được bổ sung</param>
        /// <returns>Mã nhà cung cấp cần bổ sung</returns>
        public static int AddSupplier(Supplier Data)
        {
            return SupplierDB.Add(Data);
        }

        /// <summary>
        /// Cập nhật thông tin của 1 nhà cung cấp
        /// </summary>
        /// <param name="Data">Dữ liệu của 1 nhà cung cấp</param>
        /// <returns>(True: nếu có cập nhât,False: nếu không cập nhật)</returns>
        public static bool UpdateSupplier(Supplier Data)
        {
            return SupplierDB.Update(Data);
        }

        /// <summary>
        /// Xóa 1 nhà cung cấp
        /// </summary>
        /// <param int="SupplierID"> Mã nhà cung cấp</param>
        /// <returns>(True: nếu có xóa,False: nếu không xóa)</returns>
        public static bool DeleteSupplier(int SupplierID)
        {
            return SupplierDB.Delete(SupplierID);
        }

        /// <summary>
        /// lấy thông tin 1 nhà cung cấp
        /// </summary>
        /// <param name="SupplierId">Mã nhà cung cấp</param>
        /// <returns>Thông tin của nhà cung cấp với mã đưa vào.</returns>
        public static Supplier GetSupplier(int SupplierId)
        {
            return SupplierDB.Get(SupplierId);
        }

        /// <summary>
        /// Kiểm tra xem nhà cung cấp này đã cung cấp hàng hay chưa?(kiểm tra xem nó có ở bảng hang hay ko?)
        /// </summary>
        /// <param name="SuplierID">Mã nhà cung cấp</param>
        /// <returns>(True: nếu có,False: nếu không).</returns>
        public static bool InUsedSupplier(int SuplierID)
        {
            return SupplierDB.InUsed(SuplierID);
        }
        #endregion
        #region Các nghiệp vụ liên quan đến Loại hàng

        /// <summary>
        /// Tìm kiếm, Lấy danh sách các nhà cung cấp dưới dạng phân trang.
        /// </summary>
        /// <param name="Page">Trang cần xem</param>
        /// <param name="PageSize"> Số dòng trên mối trang(=0 nếu không phân trang)</param>
        /// <param name="SearchValue">Giá trị tìm kiếm (=rỗng nếu không tìm kiếm)</param>
        /// <param name="RowCount">Output: Tổng số dòng tìm được</param>
        /// <returns></returns>
        public static List<Category> ListOfCategories(int Page, int PageSize, String SearchValue, out int RowCount)
        {
            RowCount = CategoryDB.Count(SearchValue);
            return CategoryDB.List(Page, PageSize, SearchValue).ToList();
        }

        /// <summary>
        /// tìm kiếm và lấy danh sách nhà cung cấp nếu ko phân trang.
        /// </summary>
        /// <param name="SearchValue"></param>
        /// <returns></returns>
        public static List<Category> ListofCategories(String SearchValue)
        {
            return CategoryDB.List(1, 0, SearchValue).ToList();
        }

        /// <summary>
        /// Bổ sung 1 nhà cung cấp
        /// </summary>
        /// <param name="Data">Dữ liệu nhà cung cấp được bổ sung</param>
        /// <returns>Mã nhà cung cấp cần bổ sung</returns>
        public static int AddCategory(Category Data)
        {
            return CategoryDB.Add(Data);
        }

        /// <summary>
        /// Cập nhật thông tin của 1 nhà cung cấp
        /// </summary>
        /// <param name="Data">Dữ liệu của 1 nhà cung cấp</param>
        /// <returns>(True: nếu có cập nhât,False: nếu không cập nhật)</returns>
        public static bool UpdateCategory(Category Data)
        {
            return CategoryDB.Update(Data);
        }

        /// <summary>
        /// Xóa 1 nhà cung cấp
        /// </summary>
        /// <param int="SupplierID"> Mã nhà cung cấp</param>
        /// <returns>(True: nếu có xóa,False: nếu không xóa)</returns>
        public static bool DeleteCategory(int CategoryID)
        {
            return CategoryDB.Delete(CategoryID);
        }

        /// <summary>
        /// lấy thông tin 1 nhà cung cấp
        /// </summary>
        /// <param name="SupplierId">Mã nhà cung cấp</param>
        /// <returns>Thông tin của nhà cung cấp với mã đưa vào.</returns>
        public static Category GetCategory(int CategoryID)
        {
            return CategoryDB.Get(CategoryID);
        }

        /// <summary>
        /// Kiểm tra xem nhà cung cấp này đã cung cấp hàng hay chưa?(kiểm tra xem nó có ở bảng hang hay ko?)
        /// </summary>
        /// <param name="SuplierID">Mã nhà cung cấp</param>
        /// <returns>(True: nếu có,False: nếu không).</returns>
        public static bool InUsedCategory(int CategoryID)
        {
            return CategoryDB.InUsed(CategoryID);
        }

        /// <summary>
        /// Đếm số sản phẩm sử dụng loại hàng với mã hàng đã cho.
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public static int Count(int ID)
        {
            return CategoryDB.Count(ID);
        }
        #endregion
        #region Các nghiệp vụ liên quan đến người giao hàng.

        /// <summary>
        /// Tìm kiếm, Lấy danh sách các nhà cung cấp dưới dạng phân trang.
        /// </summary>
        /// <param name="Page">Trang cần xem</param>
        /// <param name="PageSize"> Số dòng trên mối trang(=0 nếu không phân trang)</param>
        /// <param name="SearchValue">Giá trị tìm kiếm (=rỗng nếu không tìm kiếm)</param>
        /// <param name="RowCount">Output: Tổng số dòng tìm được</param>
        /// <returns></returns>
        public static List<Shipper> ListOfShippers(int Page, int PageSize, String SearchValue, out int RowCount)
        {
            RowCount = ShipperDB.Count(SearchValue);
            return ShipperDB.List(Page, PageSize, SearchValue).ToList();
        }

        /// <summary>
        /// tìm kiếm và lấy danh sách nhà cung cấp nếu ko phân trang.
        /// </summary>
        /// <param name="SearchValue"></param>
        /// <returns></returns>
        public static List<Shipper> ListOfShippers(String SearchValue)
        {
            return ShipperDB.List(1, 0, SearchValue).ToList();
        }

        /// <summary>
        /// Bổ sung 1 nhà cung cấp
        /// </summary>
        /// <param name="Data">Dữ liệu nhà cung cấp được bổ sung</param>
        /// <returns>Mã nhà cung cấp cần bổ sung</returns>
        public static int AddShipper(Shipper Data)
        {
            return ShipperDB.Add(Data);
        }

        /// <summary>
        /// Cập nhật thông tin của 1 nhà cung cấp
        /// </summary>
        /// <param name="Data">Dữ liệu của 1 nhà cung cấp</param>
        /// <returns>(True: nếu có cập nhât,False: nếu không cập nhật)</returns>
        public static bool UpdateShipper(Shipper Data)
        {
            return ShipperDB.Update(Data);
        }

        /// <summary>
        /// Xóa 1 nhà cung cấp
        /// </summary>
        /// <param int="SupplierID"> Mã nhà cung cấp</param>
        /// <returns>(True: nếu có xóa,False: nếu không xóa)</returns>
        public static bool DeleteShipper(int ShipperID)
        {
            return ShipperDB.Delete(ShipperID);
        }

        /// <summary>
        /// lấy thông tin 1 nhà cung cấp
        /// </summary>
        /// <param name="SupplierId">Mã nhà cung cấp</param>
        /// <returns>Thông tin của nhà cung cấp với mã đưa vào.</returns>
        public static Shipper GetShipper(int ShipperID)
        {
            return ShipperDB.Get(ShipperID);
        }

        /// <summary>
        /// Kiểm tra xem nhà cung cấp này đã cung cấp hàng hay chưa?(kiểm tra xem nó có ở bảng hang hay ko?)
        /// </summary>
        /// <param name="SuplierID">Mã nhà cung cấp</param>
        /// <returns>(True: nếu có,False: nếu không).</returns>
        public static bool InUsedShipper(int ShipperID)
        {
            return ShipperDB.InUsed(ShipperID);
        }
        #endregion
        #region Các nghiệp vụ liên quan đến khách hàng.

        /// <summary>
        /// Tìm kiếm, Lấy danh sách các nhà cung cấp dưới dạng phân trang.
        /// </summary>
        /// <param name="Page">Trang cần xem</param>
        /// <param name="PageSize"> Số dòng trên mối trang(=0 nếu không phân trang)</param>
        /// <param name="SearchValue">Giá trị tìm kiếm (=rỗng nếu không tìm kiếm)</param>
        /// <param name="RowCount">Output: Tổng số dòng tìm được</param>
        /// <returns></returns>
        public static List<Customer> ListOfCustomers(int Page, int PageSize, String SearchValue, out int RowCount)
        {
            RowCount = CustomerDB.Count(SearchValue);
            return CustomerDB.List(Page, PageSize, SearchValue).ToList();
        }

        /// <summary>
        /// tìm kiếm và lấy danh sách nhà cung cấp nếu ko phân trang.
        /// </summary>
        /// <param name="SearchValue"></param>
        /// <returns></returns>
        public static List<Customer> ListOfCustomers(String SearchValue)
        {
            return CustomerDB.List(1, 0, SearchValue).ToList();
        }

        /// <summary>
        /// Bổ sung 1 nhà cung cấp
        /// </summary>
        /// <param name="Data">Dữ liệu nhà cung cấp được bổ sung</param>
        /// <returns>Mã nhà cung cấp cần bổ sung</returns>
        public static int AddCustomer(Customer Data)
        {
            return CustomerDB.Add(Data);
        }

        /// <summary>
        /// Cập nhật thông tin của 1 nhà cung cấp
        /// </summary>
        /// <param name="Data">Dữ liệu của 1 nhà cung cấp</param>
        /// <returns>(True: nếu có cập nhât,False: nếu không cập nhật)</returns>
        public static bool UpdateCustomer(Customer Data)
        {
            return CustomerDB.Update(Data);
        }

        /// <summary>
        /// Xóa 1 nhà cung cấp
        /// </summary>
        /// <param int="SupplierID"> Mã nhà cung cấp</param>
        /// <returns>(True: nếu có xóa,False: nếu không xóa)</returns>
        public static bool DeleteCustomer(int CustomerID)
        {
            return CustomerDB.Delete(CustomerID);
        }

        /// <summary>
        /// lấy thông tin 1 nhà cung cấp
        /// </summary>
        /// <param name="SupplierId">Mã nhà cung cấp</param>
        /// <returns>Thông tin của nhà cung cấp với mã đưa vào.</returns>
        public static Customer GetCustomer(int CustomerID)
        {
            return CustomerDB.Get(CustomerID);
        }

        /// <summary>
        /// Kiểm tra xem nhà cung cấp này đã cung cấp hàng hay chưa?(kiểm tra xem nó có ở bảng hang hay ko?)
        /// </summary>
        /// <param name="SuplierID">Mã nhà cung cấp</param>
        /// <returns>(True: nếu có,False: nếu không).</returns>
        public static bool InUsedCustomer(int CustomerID)
        {
            return CustomerDB.InUsed(CustomerID);
        }
        #endregion
        #region Các nghiệp vụ liên quan đến nhân viên.

        /// <summary>
        /// Tìm kiếm, Lấy danh sách các nhà cung cấp dưới dạng phân trang.
        /// </summary>
        /// <param name="Page">Trang cần xem</param>
        /// <param name="PageSize"> Số dòng trên mối trang(=0 nếu không phân trang)</param>
        /// <param name="SearchValue">Giá trị tìm kiếm (=rỗng nếu không tìm kiếm)</param>
        /// <param name="RowCount">Output: Tổng số dòng tìm được</param>
        /// <returns></returns>
        public static List<Employee> ListOfEmployees(int Page, int PageSize, String SearchValue, out int RowCount)
        {
            RowCount = EmployeeDB.Count(SearchValue);
            return EmployeeDB.List(Page, PageSize, SearchValue).ToList();
        }

        /// <summary>
        /// tìm kiếm và lấy danh sách nhà cung cấp nếu ko phân trang.
        /// </summary>
        /// <param name="SearchValue"></param>
        /// <returns></returns>
        public static List<Employee> ListOfEmployees(String SearchValue)
        {
            return EmployeeDB.List(1, 0, SearchValue).ToList();
        }

        /// <summary>
        /// Bổ sung 1 nhà cung cấp
        /// </summary>
        /// <param name="Data">Dữ liệu nhà cung cấp được bổ sung</param>
        /// <returns>Mã nhà cung cấp cần bổ sung</returns>
        public static int AddEmployee(Employee Data)
        {
            return EmployeeDB.Add(Data);
        }

        /// <summary>
        /// Cập nhật thông tin của 1 nhà cung cấp
        /// </summary>
        /// <param name="Data">Dữ liệu của 1 nhà cung cấp</param>
        /// <returns>(True: nếu có cập nhât,False: nếu không cập nhật)</returns>
        public static bool UpdateEmployee(Employee Data)
        {
            return EmployeeDB.Update(Data);
        }

        /// <summary>
        /// Xóa 1 nhà cung cấp
        /// </summary>
        /// <param int="SupplierID"> Mã nhà cung cấp</param>
        /// <returns>(True: nếu có xóa,False: nếu không xóa)</returns>
        public static bool DeleteEmployee(int EmployeeID)
        {
            return EmployeeDB.Delete(EmployeeID);
        }

        /// <summary>
        /// lấy thông tin 1 nhà cung cấp
        /// </summary>
        /// <param name="SupplierId">Mã nhà cung cấp</param>
        /// <returns>Thông tin của nhà cung cấp với mã đưa vào.</returns>
        public static Employee GetEmployee(int EmployeeID)
        {
            return EmployeeDB.Get(EmployeeID);
        }

        /// <summary>
        /// Kiểm tra xem nhà cung cấp này đã cung cấp hàng hay chưa?(kiểm tra xem nó có ở bảng hang hay ko?)
        /// </summary>
        /// <param name="SuplierID">Mã nhà cung cấp</param>
        /// <returns>(True: nếu có,False: nếu không).</returns>
        public static bool InUsedEmployee(int EmployeeID)
        {
            return EmployeeDB.InUsed(EmployeeID);
        }
        #endregion

    }
}
