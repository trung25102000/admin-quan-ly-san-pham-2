﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using _19T1021288.Businnesslayers;
using _19T1021288.DataLeyers;
using _19T1021288.DomainModels;
using _19T1021288.Web.Models;

namespace _19T1021312.Web.Controllers
{
    [Authorize]
    public class CategoryController : Controller
    {
        private const int PAGE_SIZE = 5;
        private const string CATEGORY_SEARCH = "SearchCategoryCondition";
        #region Phiên bản cũ
        ///// <summary>
        ///// Giao diện hiển thị danh sách loại hàng được truyền với 1 danh sách nhà cung cấp và 1 dữ liệu ra là số hàng.
        ///// </summary>
        ///// <param name="Page">Số trang cần xem</param>
        ///// <param name="PageSize">Số dòng trên 1 trang</param>
        ///// <param name="SearchValue">Giá trị tìm kiếm</param>
        ///// <returns></returns>
        //public ActionResult Index(int Page = 1, int PageSize = 10, String SearchValue = "")
        //{
        //    int RowCount = 0;
        //    var Model = CommonDataService.ListOfCategories(Page, PageSize, SearchValue, out RowCount);

        //    int PageCount = RowCount / PageSize;
        //    if (RowCount % PageSize > 0)
        //        PageCount += 1;

        //    ViewBag.Page = Page;
        //    ViewBag.PageCount = PageCount;
        //    ViewBag.PageSize = PageSize;
        //    ViewBag.SearchValue = SearchValue;
        //    ViewBag.RowCount = RowCount;

        //    return View(Model);
        //}
        #endregion

        #region Phiên bản mới

        public ActionResult Index()
        {
            PaginationSearchInput condition = Session[CATEGORY_SEARCH] as PaginationSearchInput;
            if (condition == null)
            {
                condition = new PaginationSearchInput()
                {
                    Page = 1,
                    PageSize = PAGE_SIZE,
                    SearchValue = ""

                };
            }



            return View(condition);
        }

        /// <summary>
        /// trả về form tìm kiếm với dữ liệu ra là suppliersearchoutput
        /// </summary>
        /// <param name="condition"></param>
        /// <returns></returns>
        public ActionResult Search(PaginationSearchInput condition)
        {
            int RowCount = 0;
            var data = CommonDataService.ListOfCategories(condition.Page, condition.PageSize, condition.SearchValue, out RowCount);
            var result = new CategorySearchOutput()
            {
                Page = condition.Page,
                PageSize = condition.PageSize,
                SearchValue = condition.SearchValue,
                RowCount = RowCount,
                data = data
            };
            Session[CATEGORY_SEARCH] = condition;
            return View(result);
        }

        #endregion

        /// <summary>
        /// Giao diện để bổ sung loại hàng mới
        /// </summary>
        /// <returns></returns>
        public ActionResult Create()
        {
            ViewBag.title = "Bổ sung Loại hàng";
            var data = new Category()
            {
                CategoryID = 0
            };
            return View(data);
        }
        /// <summary>
        /// Giao diện để chỉnh sửa thông tin nhà cung cấp 
        /// </summary>
        /// <returns></returns>
        public ActionResult Edit(int ID)
        {
            try
            {
                if (ID == 0) return RedirectToAction("Index");
                var data = CommonDataService.GetCategory(ID);
                if (data == null) return RedirectToAction("Index");
                ViewBag.title = "Cập nhật Thông Tin Loại Hàng.";
                return View("Create", data);
            }
            catch(Exception ex)
            {
                // ghi lại log lối
                return Content("Có lỗi xảy ra.Vui lòng thử lại sau@!");
            }
           
        }
        /// <summary>
        /// Giao diện để xóa thông tin nhà cung cấp 
        /// </summary>
        /// <returns></returns>
        public ActionResult Delete(int ID)
        {
            try
            {
                if (ID == 0) return RedirectToAction("Index");
                if (Request.HttpMethod == "GET")
                {
                    var data = CommonDataService.GetCategory(ID);
                    if (data == null) return RedirectToAction("Index");
                    return View(data);
                }
                else
                {
                    CommonDataService.DeleteCategory(ID);
                    return RedirectToAction("Index");
                }
            }
            catch(Exception ex)
            {
                // ghi lại log lối
                return Content("Có lỗi xảy ra.Vui lòng thử lại sau@!");
            }
           
        }

        /// <summary>
        /// Lưu dữ liệu nhà cung cấp sau khi có sự thay đổi dữ liệu
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Save(Category Data)
        {
            try
            {
                // Kiểm soát đầu vào.if
                if (String.IsNullOrWhiteSpace(Data.CategoryName))
                    ModelState.AddModelError("CategoryName", "Tên không được để trống!");

                if (String.IsNullOrWhiteSpace(Data.Description))
                    ModelState.AddModelError("Description", "Mô ta không nên để trống!!");



                if (!ModelState.IsValid)
                {
                    ViewBag.title = Data.CategoryID != 0 ? "Cập nhật nhà cung cấp" : "Bổ sung nhà cung cấp";
                    return View("Create", Data);
                }

                if (Data.CategoryID == 0)
                {
                    CommonDataService.AddCategory(Data);
                }
                else CommonDataService.UpdateCategory(Data);

                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                // ghi lại log lối
                return Content("Có lỗi xảy ra.Vui lòng thử lại sau@!");
            }
        }
    }
}