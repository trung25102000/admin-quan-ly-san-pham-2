﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using _19T1021288.Businnesslayers;
using LiteCommerce.BusinessLayer;
using LiteCommerce.DomainModel;

namespace _19T1021288.Web.Controllers
{
    //[RoutePrefix("thu-nghiem")]
    public class TestController : Controller
    {
        // GET: Test
        //[Route("xin-chao/{Name}/{age?}")]
        // public string SayHello(string Name,int age=18)
        // {
        //     return $"Hello {Name}, {age} yead old.";
        // }
        [HttpGet]
        public ActionResult Input()
        {

            int RowCount = 0;
            List<Product> datapProduct = ProductDataService.ListProducts(1, 5,"", 0, 0, out RowCount);
         
           
            return Json(datapProduct, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Input(person p)
        {
            dynamic data = new
            {
                Name = p.Name,
                BirthDate = String.Format("{0:dd/MM/yyyy}",p.BirthDate),
                Salary = p.Salary
            };
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public string TestDate(DateTime value)
        {
            DateTime d = value;
            return string.Format("{0:dd/MM/yyyy}",d);
        }

    }
    public class person
    {
        public string Name { get; set; }

        public DateTime BirthDate { get; set; }

        public float Salary { get; set; }
    }
}