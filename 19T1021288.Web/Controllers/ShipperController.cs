﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using _19T1021288.Web.Models;
using _19T1021288.DomainModels;
using _19T1021288.DataLeyers;
using _19T1021288.Businnesslayers;

namespace _19T1021288.Web.Controllers
{
    [Authorize]
    public class ShipperController : Controller
    {
        private const int PAGE_SIZE = 5;
        private const string SHIPPER_SEARCH = "SearchShipperCondition";
        #region Phiên bản mới
        public ActionResult Index()
        {

            PaginationSearchInput condition = Session[SHIPPER_SEARCH] as PaginationSearchInput;
            if (condition == null)
            {
                condition = new PaginationSearchInput()
                {
                    Page = 1,
                    PageSize = PAGE_SIZE,
                    SearchValue = ""

                };
            }



            return View(condition);
        }

        /// <summary>
        /// trả về form tìm kiếm với dữ liệu ra là suppliersearchoutput
        /// </summary>
        /// <param name="condition"></param>
        /// <returns></returns>
        public ActionResult Search(PaginationSearchInput condition)
        {
            int RowCount = 0;
            var data = CommonDataService.ListOfShippers(condition.Page, condition.PageSize, condition.SearchValue, out RowCount);
            var result = new ShipperSearchOutput()
            {
                Page = condition.Page,
                PageSize = condition.PageSize,
                SearchValue = condition.SearchValue,
                RowCount = RowCount,
                data = data
            };
            Session[SHIPPER_SEARCH] = condition;
            return View(result);
        }

        #endregion

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ActionResult Create()
        {
            var data = new Shipper()
            {
                ShipperID = 0
            };
            return View( data);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ActionResult edit(int ID)
        {
            try
            {
                if (ID == 0) return RedirectToAction("Index");

                var data = CommonDataService.GetShipper(ID);

                if (data == null) return RedirectToAction("Index");

                ViewBag.title = "Cập nhật thông tin người giao hàng";

                return View("Create",data);
            }
            catch (Exception ex)
            {
                // ghi lại log lối
                return Content("Có lỗi xảy ra.Vui lòng thử lại sau!");
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ActionResult Delete(int ID)
        {
            try
            {
                if (ID == 0) return RedirectToAction("Index");
                if (Request.HttpMethod == "GET")
                {
                    var data = CommonDataService.GetShipper(ID);
                    if (data == null) return RedirectToAction("Index");
                    return View(data);
                }
                else
                {
                    CommonDataService.DeleteShipper(ID);
                    return RedirectToAction("Index");
                }
            }
            catch (Exception ex)
            {
                // ghi lại log lối
                return Content("Có lỗi xảy ra.Vui lòng thử lại sau@!");
            }
        }

        /// <summary>
        /// Lưu dữ liệu nhà cung cấp sau khi có sự thay đổi dữ liệu
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Save(Shipper Data)
        {
            try
            {
                // Kiểm soát đầu vào.if
                if (String.IsNullOrWhiteSpace(Data.ShipperName))
                    ModelState.AddModelError("ShipperName", "Tên không được để trống!");


                if (!ModelState.IsValid)
                {
                    ViewBag.title = Data.ShipperID != 0 ? "Cập nhật người giao hàng" : "Bổ sung người giao hàng";
                    return View("Create", Data);
                }

                if (Data.ShipperID == 0)
                {
                    CommonDataService.AddShipper(Data);
                }
                else CommonDataService.UpdateShipper(Data);

                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                // ghi lại log lối
                return Content("Có lỗi xảy ra.Vui lòng thử lại sau!"+ex.GetType());
            }


        }

    }
}