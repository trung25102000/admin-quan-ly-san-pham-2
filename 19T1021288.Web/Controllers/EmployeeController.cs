﻿using _19T1021288.DomainModels;
using _19T1021288.DataLeyers;
using _19T1021288.Businnesslayers;
using _19T1021288.Web;
using _19T1021288.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace _19T1021312.Web.Controllers
{
    [Authorize]
    public class EmployeeController : Controller
    {

        /// 
        /// </summary>
        /// <returns></returns>
        private const int PAGE_SIZE = 6;
        private const string EMPLOYEE_SEARCH = "SearchEmployeeCondition";
        /// <summary>
        /// Giao diện hiển thị danh sách nhà cung cấp được truyền với 1 danh sách nhà cung cấp và 1 dữ liệu ra là số hàng.
        /// </summary>
        /// <param name="Page">Số trang cần xem</param>
        /// <param name="PageSize">Số dòng trên 1 trang</param>
        /// <param name="SearchValue">Giá trị tìm kiếm</param>
        /// <returns></returns>
        #region Phiên bản cũ
        //public ActionResult Index(int Page=1,int PageSize=20,String SearchValue="")
        //{

        //int RowCount = 0;
        //var Model = CommonDataService.ListOfSuppliers(Page,PageSize,SearchValue,out RowCount);

        //int PageCount = RowCount / PageSize;
        //if (RowCount % PageSize > 0)
        //    PageCount += 1;

        //ViewBag.Page = Page;
        //ViewBag.PageCount = PageCount;
        //ViewBag.PageSize = PageSize;
        //ViewBag.SearchValue = SearchValue;
        //ViewBag.RowCount = RowCount;

        //return View(Model);

        // }
        #endregion
        public ActionResult Index()
        {
            PaginationSearchInput condition = Session[EMPLOYEE_SEARCH] as PaginationSearchInput;
            if (condition == null)
            {
                condition = new PaginationSearchInput()
                {
                    Page = 1,
                    PageSize = PAGE_SIZE,
                    SearchValue = ""

                };
            }
            return View(condition);
        }

        /// <summary>
        /// Hiển thị thông tin chi tiết nhân viên.
        /// </summary>
        /// <param name="UsedID"></param>
        /// <returns></returns>
        public ActionResult Details(string id)
        {
            int EmployeeID = Convert.ToInt32(id);

            Employee data = CommonDataService.GetEmployee(EmployeeID);

            return View(data);
        }    

        /// <summary>
        /// 
        /// </summary>
        /// <param name="condition"></param>
        /// <returns></returns>
        public ActionResult Search(PaginationSearchInput condition)
        {
            int RowCount = 0;
            var data = CommonDataService.ListOfEmployees(condition.Page, condition.PageSize, condition.SearchValue, out RowCount);
            var result = new employeeSearchOutput()
            {
                Page = condition.Page,
                PageSize = condition.PageSize,
                SearchValue = condition.SearchValue,
                RowCount = RowCount,
                data = data
            };
            Session[EMPLOYEE_SEARCH] = condition;
            return View(result);
        }

        /// <summary>
        /// giao diện bổ sung một nhân viên mới
        /// </summary>
        /// <returns></returns>
        public ActionResult Create()
        {
            ViewBag.title = "Bổ sung nhân viên";
            var data = new Employee()
            {
                EmployeeID = 0
            };
            return View("Edit", data);
        }

        /// <summary>
        /// Giao diện để chỉnh sửa thông tin nhân viên
        /// </summary>
        /// <returns></returns>
        public ActionResult Edit(int id = 0)
        {
            if (id == 0)
                return RedirectToAction("Index");
            var data = CommonDataService.GetEmployee(id);
            if (data == null)
                return RedirectToAction("Index");
            ViewBag.title = "Cập nhật thông tin nhân viên";
            return View(data);
        }

        /// <summary>
        /// Giao diện để xóa thông tin nhân viên
        /// </summary>
        /// <returns></returns>
        public ActionResult Delete(string id)
        {
            int EmployeeID = Convert.ToInt32(id);
            if (Request.HttpMethod == "GET")
            {
                var data = CommonDataService.GetEmployee(EmployeeID);
                return View(data);
            }
            else
            {
                CommonDataService.DeleteEmployee(EmployeeID);
                return RedirectToAction("Index");
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Data"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Save(Employee Data, string birthDay, HttpPostedFileBase uploadPhoto)
        {

            DateTime? d = SelectListHelper.DMYStringToDateTime(birthDay);

            if (d == null)
                ModelState.AddModelError("BirthDate", "*");
            else
                Data.BirthDate = d.Value;
            /// Kiểm tra đầu vào hợp lệ 
            if (string.IsNullOrWhiteSpace(Data.FirstName))
                ModelState.AddModelError("FirstName", " Vui lòng nhập họ và tên đệm");
            if (string.IsNullOrWhiteSpace(Data.LastName))
                ModelState.AddModelError("LastName", "Vui lòng nhập tên");
            if (string.IsNullOrWhiteSpace(Data.Email))
                ModelState.AddModelError("Email", "Vui lòng nhập Email");
            if (string.IsNullOrWhiteSpace(Data.Notes))
                ModelState.AddModelError("Notes", "Vui lòng nhập ghi chú");

            if (string.IsNullOrWhiteSpace(Data.Photo))
                Data.Photo = "";
            if (uploadPhoto != null)
            {
                string path = Server.MapPath("~/Images");
                string FileName = $"{DateTime.Now.Ticks}_{uploadPhoto.FileName}";
                string filepath = System.IO.Path.Combine(path, FileName);
                uploadPhoto.SaveAs(filepath);
                Data.Photo = FileName;
            }

            if (!ModelState.IsValid)
            {
                ViewBag.Title = Data.EmployeeID == 0 ? "Bổ sung nhân viên" : "Cập nhật thông tin nhân viên";
                return View("Edit", Data);
            }

            if (Data.EmployeeID == 0)
            {
                CommonDataService.AddEmployee(Data);
            }
            else CommonDataService.UpdateEmployee(Data);
           
            return RedirectToAction("Index");




        }
    }
}