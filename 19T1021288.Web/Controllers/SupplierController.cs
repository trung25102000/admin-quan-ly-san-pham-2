﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using _19T1021288.DomainModels;
using _19T1021288.DataLeyers;
using _19T1021288.Businnesslayers;
using _19T1021288.Web.Models;

namespace _19T1021312.Web.Controllers
{
    [Authorize]
    public class SupplierController : Controller
    {
        private const int PAGE_SIZE= 5;
        private const string SUPPLIER_SEARCH = "SearchSupplierCondition";

        /// <summary>
        /// Giao diện hiển thị danh sách nhà cung cấp được truyền với 1 danh sách nhà cung cấp và 1 dữ liệu ra là số hàng.
        /// </summary>
        /// <param name="Page">Số trang cần xem</param>
        /// <param name="PageSize">Số dòng trên 1 trang</param>
        /// <param name="SearchValue">Giá trị tìm kiếm</param>
        /// <returns></returns>
        #region Phiên bản cũ
        //public ActionResult Index(int Page=1,int PageSize=20,String SearchValue="")
        //{

        //int RowCount = 0;
        //var Model = CommonDataService.ListOfSuppliers(Page,PageSize,SearchValue,out RowCount);

        //int PageCount = RowCount / PageSize;
        //if (RowCount % PageSize > 0)
        //    PageCount += 1;

        //ViewBag.Page = Page;
        //ViewBag.PageCount = PageCount;
        //ViewBag.PageSize = PageSize;
        //ViewBag.SearchValue = SearchValue;
        //ViewBag.RowCount = RowCount;

        //return View(Model);

        // }
        #endregion

        #region Phiên bản mới
        public ActionResult Index()
        {
            PaginationSearchInput condition = Session[SUPPLIER_SEARCH] as PaginationSearchInput;
            if(condition==null)
            {
                condition = new PaginationSearchInput()
                {
                    Page = 1,
                    PageSize = PAGE_SIZE,
                    SearchValue = ""

                };
            }    

           

            return View(condition);
        }

        /// <summary>
        /// trả về form tìm kiếm với dữ liệu ra là suppliersearchoutput
        /// </summary>
        /// <param name="condition"></param>
        /// <returns></returns>
        public ActionResult Search(PaginationSearchInput condition)
        {
            int RowCount = 0;
            var data = CommonDataService.ListOfSuppliers(condition.Page, condition.PageSize, condition.SearchValue,out RowCount);
            var result = new SupplierSearchOutput()
            {
                Page = condition.Page,
                PageSize = condition.PageSize,
                SearchValue = condition.SearchValue,
                RowCount = RowCount,
                Data = data
            };
            Session[SUPPLIER_SEARCH] = condition;
            return View(result);
        }

        #endregion


        /// <summary>
        /// Giao diện để bổ sung nhà cung cấp mới
        /// </summary>
        /// <returns></returns>
        public ActionResult Create()
        {
            ViewBag.title = "Bổ sung nhà cung cấp";
            var data = new Supplier()
            {
                SupplierID = 0
            };
            return View("Edit",data);
        }

        /// <summary>
        /// Giao diện để chỉnh sửa thông tin nhà cung cấp 
        /// </summary>
        /// <returns></returns>

        #region Các phiên bản edit khác nhau
        //public ActionResult Edit(String ID)
        //{
        //    int SupplierID = Convert.ToInt32(ID);
        //    var data = CommonDataService.GetSupplier(SupplierID); ;
        //    ViewBag.title = "Cập nhật nhà cung cấp";
        //    return View(data);
        //}

        //public ActionResult Edit(int? ID)
        //{
        //    if (ID == null) return RedirectToAction("Index");
        //    var data = CommonDataService.GetSupplier(ID);
        //    ViewBag.title = "Cập nhật nhà cung cấp";
        //    return View(data);
        //}
        #endregion

        public ActionResult Edit(int ID)
        {
            try
            {
                if (ID == 0) return RedirectToAction("Index");

                var data = CommonDataService.GetSupplier(ID);

                if (data == null) return RedirectToAction("Index");

                ViewBag.title = "Cập nhật nhà cung cấp";

                return View(data);
            }
            catch (Exception ex)
            {
                // ghi lại log lối
                return Content("Có lỗi xảy ra.Vui lòng thử lại sau@!");
            }
           
        }

        /// <summary>
        /// Giao diện để xóa thông tin nhà cung cấp 
        /// </summary>
        /// <returns></returns>
        public ActionResult Delete(int ID)
        {
            try
            {
                if (ID == 0) return RedirectToAction("Index");
                if (Request.HttpMethod == "GET")
                {
                    var data = CommonDataService.GetSupplier(ID);
                    if (data == null) return RedirectToAction("Index");
                    return View(data);
                }
                else
                {
                    CommonDataService.DeleteSupplier(ID);
                    return RedirectToAction("Index");
                }
            }
            catch(Exception ex)
            {
                // ghi lại log lối
                return Content("Có lỗi xảy ra.Vui lòng thử lại sau@!");
            }
            
        }

        /// <summary>
        /// Lưu dữ liệu nhà cung cấp sau khi có sự thay đổi dữ liệu
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Save(Supplier Data)
        {
            try
            {
                // Kiểm soát đầu vào.if
                if (String.IsNullOrWhiteSpace(Data.SupplierName))
                    ModelState.AddModelError("SupplierName", "Tên không được để trống!");

                if (String.IsNullOrWhiteSpace(Data.ContactName))
                    ModelState.AddModelError("ContactName", "Tên giao dịch không được để trống!");
              
                if (String.IsNullOrWhiteSpace(Data.Country))
                    ModelState.AddModelError("Country", "Vui lòng chọn quốc gia!");

                if (String.IsNullOrWhiteSpace(Data.Address))
                    ModelState.AddModelError("Address", "Vui lòng chọn địa chỉ!");


                if (!ModelState.IsValid)
                {
                    ViewBag.title = Data.SupplierID!=0 ? "Cập nhật nhà cung cấp": "Bổ sung nhà cung cấp";
                    return View("Edit",Data);
                }    

                if (Data.SupplierID == 0)
                {
                    CommonDataService.AddSupplier(Data);
                }
                else CommonDataService.UpdateSupplier(Data);

                return RedirectToAction("Index");
            }
            catch(Exception ex)
            {
                // ghi lại log lối
                return Content("Có lỗi xảy ra.Vui lòng thử lại sau@!");
            }

         
        }
    }
}