﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using _19T1021288.DomainModels;
using _19T1021288.Businnesslayers;
using System.Web.Security;

namespace _19T1021312.Web.Controllers
{
    //hàm đặt trước khi chạy vào hàm.
    // hàm này có tác dụng phải đăng nhập mới vào đc
    [Authorize]
    public class AccoutController : Controller
    { 
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>       
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        // phương thức get sẽ vào đây..tức là chạy lần đâu, và chưa có giá trị.
        [AllowAnonymous]
        // cho phép chạy vào hàm cho dù có hàm điều kiện [authorize] ở ngoài.
        public ActionResult Login()
        {
            return View();
        }

        /// <summary>
        /// Trang đăng nhập.
        /// </summary>
        /// <returns></returns>
        [ValidateAntiForgeryToken]
        // Kiểm tra token từ form gửi lên..nếu đúng thì mới chạy vào hàm...bào mật để tránh hackker sử dụng đăng nhập chạy nh lần để lấy tài khoản.
        [AllowAnonymous]
        [HttpPost]
        public ActionResult Login(string UserName="",string Password="")
        {
            if (Request.HttpMethod == "GET")
            {
                return View();
            }
            if(string.IsNullOrWhiteSpace(UserName) || string.IsNullOrWhiteSpace(Password))
            {
                ViewBag.UserName = UserName;
                ModelState.AddModelError("", "vui lòng nhập đầy đủ thông tin!");
                return View();
            }

            var userAccount = UserAccountService.Authorize(AccountTypes.Employee, UserName, Password);

            if (userAccount == null)
            {
                ViewBag.UserName = UserName;
                ModelState.AddModelError("", "Đăng nhập thất bại!");
                return View();
            }

            string cookieValue = Newtonsoft.Json.JsonConvert.SerializeObject(userAccount);
            FormsAuthentication.SetAuthCookie(cookieValue,false);
            return RedirectToAction("Index", "Home");
  
        }

        /// <summary>
        /// Đăng xuất
        /// </summary>
        /// <returns></returns>
        public ActionResult Logout()
        {
            Session.Clear();
            FormsAuthentication.SignOut();
            return RedirectToAction("Login");
        }

        /// <summary>
        /// Đổi mật khẩu
        /// </summary>
        /// <param name="id">Mã nhân viên cần đổi pass</param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult ChangePassword(string ID)
        {
            int id = Convert.ToInt32(ID);
            Employee data = null;
            data = CommonDataService.GetEmployee(id);

            return View(data);
        }

        /// <summary>
        /// Đổi mật khẩu với phương thức post
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult ChangePassword(string id,string OldPassword,string NewPassword,string ConfirmPassword)
        {    
            int ID = Convert.ToInt32(id);
            Employee Empl = CommonDataService.GetEmployee(ID);
            if (Request.HttpMethod == "GET")
            {
                var data = CommonDataService.GetEmployee(ID);
                return View(data);
            }

            if (string.IsNullOrWhiteSpace(OldPassword) || string.IsNullOrWhiteSpace(NewPassword) || string.IsNullOrWhiteSpace(ConfirmPassword))
            {
                ModelState.AddModelError("", "vui lòng nhập đầy đủ thông tin!");
                return View(Empl);
            }
            if(Empl.Password.Equals(OldPassword)!=true)
            {
                ModelState.AddModelError("", "Mật khẩu cũ chưa chính xác!");
                return View(Empl);
            }
            if(!NewPassword.Equals(ConfirmPassword))
            {
                ModelState.AddModelError("", "Mật khẩu xác nhân không trùng lặp");
                return View(Empl);
            }

            var Chane = UserAccountService.ChangePassword(AccountTypes.Employee,Empl.Email, OldPassword, NewPassword);

             return RedirectToAction("Index", "Home");

        }
    }
}