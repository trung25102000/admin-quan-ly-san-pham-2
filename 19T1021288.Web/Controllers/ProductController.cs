﻿using LiteCommerce.BusinessLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using _19T1021288.Web.Models;
using LiteCommerce.DomainModel;

namespace LiteCommerce.Web.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Authorize]
    [RoutePrefix("product")]
    public class ProductController : Controller
    {
        private const int PAGE_SIZE = 15;
        private const string PRODUCT_SEARCH = "SearchProductCondition";

        /// <summary>
        /// Tìm kiếm, hiển thị mặt hàng dưới dạng phân trang
        /// </summary>
        /// <returns></returns>
        #region Phiên bản cũ
        //public ActionResult Index(int page = 1, int pageSize=5 , string searchValue = "", int categoryID = 0, int supplierID = 0)
        //{
        //    int rowcount = 0;
        //    var model = ProductDataService.ListProducts(page, pageSize, searchValue, categoryID,supplierID,out rowcount);

        //    int pagecount = rowcount / pageSize;
        //    if (rowcount % pageSize > 0)
        //        pagecount += 1;

        //    ViewBag.page = page;
        //    ViewBag.pagecount = pagecount;
        //    ViewBag.pagesize = pageSize;
        //    ViewBag.searchvalue = searchValue;
        //    ViewBag.rowcount = rowcount;

        //    return View(model);
        //}
        #endregion

        #region Phiên bản mới của search
        public ActionResult Index()
        {
            ProductSearchInput condition = Session[PRODUCT_SEARCH] as ProductSearchInput;
            if (condition == null)
            {
                condition = new ProductSearchInput()
                {
                    Page = 1,
                    PageSize = PAGE_SIZE,
                    SearchValue = "",
                    CategoryID = 0,
                    SupplierID = 0

                };
            }
            return View(condition);
        }

        /// <summary>
        /// trả về form tìm kiếm với dữ liệu ra là suppliersearchoutput
        /// </summary>
        /// <param name="condition"></param>
        /// <returns></returns>
        public ActionResult Search(ProductSearchInput condition)
        {
            int RowCount = 0;
            List<Product> datapProduct = ProductDataService.ListProducts(condition.Page, condition.PageSize, condition.SearchValue, condition.CategoryID, condition.SupplierID, out RowCount);
            var result = new ProductSearchOutput()
            {
                Page = condition.Page,
                PageSize = condition.PageSize,
                SearchValue = condition.SearchValue,
                RowCount = RowCount,
                data=datapProduct
            };
            Session[PRODUCT_SEARCH] = condition;
            //return Json(datapProduct[73].ProductName, JsonRequestBehavior.AllowGet);
            return View(result);
        }

        #endregion

        #region tác vụ của sản phảm

        /// <summary>
        /// Tạo mặt hàng mới
        /// </summary>
        /// <returns></returns>
        public ActionResult Create()
        {
            var data = new Product()
            {
                ProductID = 0
            };
            return View(data);
        }

        /// <summary>
        /// Lưu sản phảm mới
        /// </summary>
        /// <param name="Data"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Save(Product Data, HttpPostedFileBase uploadPhoto)
        {
            /// Kiểm tra đầu vào hợp lệ 
            if (string.IsNullOrWhiteSpace(Data.ProductName))
                ModelState.AddModelError("ProductName", " Vui lòng nhập tên Hàng");
            if (string.IsNullOrWhiteSpace(Data.Unit))
                ModelState.AddModelError("Unit", "Vui lòng nhập đơn vị tính");
            if (Data.CategoryID == 0)
                ModelState.AddModelError("CategoryID", "Vui lòng chọn loại hàng tương ứng");
            if (Data.SupplierID == 0)
                ModelState.AddModelError("SupplierID", "Vui lòng chọn nhà cung cấp tương ứng");

            if (string.IsNullOrWhiteSpace(Data.Photo))
                Data.Photo = "";
            if (uploadPhoto != null)
            {
                string path = Server.MapPath("~/Images");
                string FileName = $"{DateTime.Now.Ticks}_{uploadPhoto.FileName}";
                string filepath = System.IO.Path.Combine(path, FileName);
                uploadPhoto.SaveAs(filepath);
                Data.Photo = FileName;
            }
            if (!ModelState.IsValid)
            {
                return View("Create", Data);
            }
            if (Data.ProductID == 0)
            {
                int temp = ProductDataService.AddProduct(Data);
                if(!string.IsNullOrWhiteSpace(Data.Photo))
                {
                    ProductDataService.AddPhoto(new ProductPhoto
                    {
                        ProductID = temp,
                        Photo = Data.Photo,
                        DisplayOrder = 1,
                        Description = "",
                        IsHidden = false
                    });
                }    
                
            }
            return RedirectToAction("Index");




        }

        /// <summary>
        /// Lưu thay đổi sản phẩm
        /// </summary>
        /// <param name="Prod"></param>
        /// <param name="uploadPhoto"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SaveEdit(ProductEditModel Prod, HttpPostedFileBase uploadPhoto,string DataPhoto)
        {

            // Kiểm tra đầu vào hợp lệ
           
            if (string.IsNullOrWhiteSpace(Prod.Data.ProductName))
                ModelState.AddModelError("ProductName", " Vui lòng nhập tên Hàng");
            if (string.IsNullOrWhiteSpace(Prod.Data.Unit))
                ModelState.AddModelError("Unit", "Vui lòng nhập đơn vị tính");
            if (Prod.Data.CategoryID == 0)
                ModelState.AddModelError("CategoryID", "Vui lòng chọn loại hàng tương ứng");
            if (Prod.Data.SupplierID == 0)
                ModelState.AddModelError("SupplierID", "Vui lòng chọn nhà cung cấp tương ứng");

            if (string.IsNullOrWhiteSpace(DataPhoto))
            {
                Prod.Data.Photo = "";
            }
            else
            {
                Prod.Data.Photo = DataPhoto;
                var ProductPhoto = ProductDataService.ListPhotos(Prod.Data.ProductID);
                if (ProductPhoto.Count()==0)
                {
                    ProductDataService.AddPhoto(new ProductPhoto
                    {
                        ProductID = Prod.Data.ProductID,
                        Photo = Prod.Data.Photo,
                        Description = "",
                        IsHidden = false,
                        DisplayOrder = 1
                    });
                }
                else
                {
                    var temp = ProductDataService.PhotoId(Prod.Data.ProductID);
                    ProductDataService.UpdatePhoto(new ProductPhoto
                    {
                        PhotoID=temp.PhotoID,
                        ProductID = temp.ProductID,
                        Photo = Prod.Data.Photo,
                        Description = temp.Description,
                        IsHidden = temp.IsHidden,
                        DisplayOrder = 1
                    });
                }
                
            }    
            var model = new ProductEditModel()
            {
                Data = ProductDataService.GetProduct(Prod.Data.ProductID),
                ListProductAttribute = ProductDataService.ListAttributes(Prod.Data.ProductID),
                ListProductPhoto = ProductDataService.ListPhotos(Prod.Data.ProductID)
            };

            if (uploadPhoto != null)
            {
                string path = Server.MapPath("~/images");
                string filename = $"{DateTime.Now.Ticks}_{uploadPhoto.FileName}";
                string filepath = System.IO.Path.Combine(path, filename);
                uploadPhoto.SaveAs(filepath);       
                Prod.Data.Photo = filename;
                
                var ProductPhoto = ProductDataService.ListPhotos(Prod.Data.ProductID);
                if (ProductPhoto.Count()==0)
                {
                    ProductDataService.AddPhoto(new ProductPhoto
                    {
                        ProductID = Prod.Data.ProductID,
                        Photo = Prod.Data.Photo,
                        Description = "",
                        IsHidden = false,
                        DisplayOrder = 1
                    });
                }
                else
                {
                    var temp = ProductDataService.PhotoId(Prod.Data.ProductID);
                    ProductDataService.UpdatePhoto(new ProductPhoto
                    {
                        PhotoID = temp.PhotoID,
                        ProductID = temp.ProductID,
                        Photo = Prod.Data.Photo,
                        Description = temp.Description,
                        IsHidden = temp.IsHidden,
                        DisplayOrder = 1
                    });
                }

            }
            if (!ModelState.IsValid)
            {
                return View("Edit", model);
            }
            if (Prod.Data.ProductID != 0)
            {
                ProductDataService.UpdateProduct(Prod.Data);
                var model2 = new ProductEditModel()
                {
                    Data = ProductDataService.GetProduct(Prod.Data.ProductID),
                    ListProductAttribute = ProductDataService.ListAttributes(Prod.Data.ProductID),
                    ListProductPhoto = ProductDataService.ListPhotos(Prod.Data.ProductID)
                };
                return View("Edit", model2);
            }
            return View("Edit", model);


        }

        /// <summary>
        /// Cập nhật thông tin mặt hàng, 
        /// Hiển thị danh sách ảnh và thuộc tính của mặt hàng, điều hướng đến các chức năng
        /// quản lý ảnh và thuộc tính của mặt hàng
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>        
        public ActionResult Edit(int id = 0)
        {

            var model = new ProductEditModel()
            {
                Data = ProductDataService.GetProduct(id),
                ListProductAttribute = ProductDataService.ListAttributes(id),
                ListProductPhoto = ProductDataService.ListPhotos(id)
            };

            return View(model);


        }

        /// <summary>
        /// Xóa mặt hàng
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>        
        public ActionResult Delete(int id = 0)
        {
            if (Request.HttpMethod == "GET")
            {
                var data = ProductDataService.GetProduct(id);
                return View(data);
            }
            else
            {
             
                ProductDataService.DeleteProduct(id);
                //return Json(id, JsonRequestBehavior.AllowGet);
                
                return RedirectToAction("Index");
               
            }
        }
        #endregion

        #region Ảnh mặt hàng.
        /// <summary>
        /// Các chức năng quản lý ảnh của mặt hàng
        /// </summary>
        /// <param name="method"></param>
        /// <param name="productID"></param>
        /// <param name="photoID"></param>
        /// <returns></returns>
        [Route("photo/{method?}/{productID?}/{photoID?}")]
        public ActionResult Photo(string method = "add", int productID = 0, long photoID = 0)
        {
            switch (method)
            {
                case "add":
                    ViewBag.Title = "Bổ sung ảnh";
                    var data = new ProductPhoto()
                    {
                        PhotoID = 0,
                        ProductID = productID
                    };
                    return View(data);
                case "edit":
                    ViewBag.Title = "Thay đổi ảnh";
                    var ProductPhoto = ProductDataService.GetPhoto(photoID);

                    return View(ProductPhoto);
                case "delete":
                   
                    ProductPhoto productPhotoo = ProductDataService.GetPhoto(photoID);
                    if(productPhotoo.DisplayOrder==1)
                    {
                        var product = ProductDataService.GetProduct(productID);
                        ProductDataService.UpdateProduct(new Product
                        {
                            ProductID=product.ProductID,
                            ProductName=product.ProductName,
                            Photo="",
                            Price=product.Price,
                            Unit=product.Unit,
                            CategoryID=product.CategoryID,
                            SupplierID=product.SupplierID
                        });
                    }
                    ProductDataService.DeletePhoto(photoID);
                    var model = new ProductEditModel()
                    {
                        Data = ProductDataService.GetProduct(productID),
                        ListProductAttribute = ProductDataService.ListAttributes(productID),
                        ListProductPhoto = ProductDataService.ListPhotos(productID)
                    };
                    return View("Edit", model);
                default:
                    return RedirectToAction("Index");
            }
        }

        /// <summary>
        /// Lưu ảnh sản phảm mới của chức năng thêm.
        /// </summary>
        /// <param name="Data"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SavePhoto(ProductPhoto Data, HttpPostedFileBase uploadPhoto)
        {
           
            if(string.IsNullOrWhiteSpace(Data.Photo) && uploadPhoto==null)
                ModelState.AddModelError("Photo", " Vui lòng chọn ảnh");
            if (string.IsNullOrWhiteSpace(Data.Description))
                ModelState.AddModelError("Description", "Bạn nên chú thích ảnh");
            if (Data.DisplayOrder == 0 )
                ModelState.AddModelError("DisplayOrder", "Vui lòng chọn thứ tự tương ứng");
            if(ProductDataService.inUsedDisplayOrder(Data.ProductID, Data.DisplayOrder) && Data.PhotoID==0)
                ModelState.AddModelError("DisplayOrder", "Thứ tự ảnh trên đã có ảnh!");
            if (uploadPhoto != null)
            {
                string path = Server.MapPath("~/Images");
                string FileName = $"{DateTime.Now.Ticks}_{uploadPhoto.FileName}";
                string filepath = System.IO.Path.Combine(path, FileName);
                uploadPhoto.SaveAs(filepath);
                Data.Photo = FileName;
             
            }
            if (!ModelState.IsValid)
            {
                return View("photo", Data);
            }
            if (Data.PhotoID == 0)
            {
                ProductDataService.AddPhoto(Data);
                if (Data.DisplayOrder == 1)
                {
                    var product = ProductDataService.GetProduct(Data.ProductID);
                    ProductDataService.UpdateProduct(new Product
                    {
                        ProductID = product.ProductID,
                        ProductName = product.ProductName,
                        Photo = Data.Photo,
                        Price = product.Price,
                        Unit = product.Unit,
                        CategoryID = product.CategoryID,
                        SupplierID = product.SupplierID
                    });
                }
            }
            else
            {
                ProductDataService.UpdatePhoto(Data);
                if(Data.DisplayOrder==1)
                {
                    Product P = ProductDataService.GetProduct(Data.ProductID);
                    P.Photo = Data.Photo;
                    ProductDataService.UpdateProduct(P);
                }    
               
            }
            var model = new ProductEditModel()
            {
                Data = ProductDataService.GetProduct(Data.ProductID),
                ListProductAttribute = ProductDataService.ListAttributes(Data.ProductID),
                ListProductPhoto = ProductDataService.ListPhotos(Data.ProductID)
            };
            return View("Edit",model);




        }
        #endregion

        #region Thuộc tính mặt hàng
        /// <summary>
        /// Các chức năng quản lý thuộc tính của mặt hàng
        /// </summary>
        /// <param name="method"></param>
        /// <param name="productID"></param>
        /// <param name="attributeID"></param>
        /// <returns></returns>
        [Route("attribute/{method?}/{productID}/{attributeID?}")]
        public ActionResult Attribute(string method = "add", int productID = 0, long attributeID = 0)
        {
            switch (method)
            {
                case "add":
                    ViewBag.Title = "Bổ sung thuộc tính";
                    var data = new ProductAttribute()
                    {
                        AttributeID = 0,
                        ProductID = productID
                    };
                    return View(data);
                case "edit":
                    ViewBag.Title = "Thay đổi thuộc tính";
                    var ProductAttribute = ProductDataService.GetAttribute(attributeID);
                    return View(ProductAttribute);
                case "delete":
                    ProductDataService.DeleteAttribute(attributeID);
                    var model = new ProductEditModel()
                    {
                        Data = ProductDataService.GetProduct(productID),
                        ListProductAttribute = ProductDataService.ListAttributes(productID),
                        ListProductPhoto = ProductDataService.ListPhotos(productID)
                    };
                    return RedirectToAction($"Edit/{productID}"); //return RedirectToAction("Edit", new { productID = productID });
                default:
                    return RedirectToAction("Index");
            }
        }

        /// <summary>
        /// Lưu thuộc tính sản phảm mới.
        /// </summary>
        /// <param name="Data"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SaveAttribute(ProductAttribute Data)
        {
            /// Kiểm tra đầu vào hợp lệ 
            if (string.IsNullOrWhiteSpace(Data.AttributeName))
                ModelState.AddModelError("AttributeName", "Tên thuộc tính ko để trống!");
            if (string.IsNullOrWhiteSpace(Data.AttributeValue))
                ModelState.AddModelError("AttributeValue", "Nhập giá trị thuộc tính");
            if (Data.DisplayOrder == 0)
                ModelState.AddModelError("DisplayOrder", "Vui lòng chọn thứ tự tương ứng");

            if (!ModelState.IsValid)
            {
                return View("Attribute", Data);
            }
            if (Data.AttributeID == 0)
            {
                ProductDataService.AddAttribute(Data);
            }
            else ProductDataService.UpdateAttribute(Data);
            var model = new ProductEditModel()
            {
                Data = ProductDataService.GetProduct(Data.ProductID),
                ListProductAttribute = ProductDataService.ListAttributes(Data.ProductID),
                ListProductPhoto = ProductDataService.ListPhotos(Data.ProductID)
            };
            return View("Edit", model);




        }
        #endregion
    }
}