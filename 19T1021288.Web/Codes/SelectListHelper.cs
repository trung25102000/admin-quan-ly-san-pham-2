﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using _19T1021288.DomainModels;
using _19T1021288.Businnesslayers;
using System.Globalization;
using System.Data.SqlClient;
using System.Data;
using _19T1021288.DataLeyers.SQLServer;
using LiteCommerce.DomainModel;

namespace _19T1021288.Web
{
    /// <summary>
    /// Cung cấp 1 số code tiện ích có liên quan đến SelectList
    /// </summary>
    public static class SelectListHelper
    {

        /// <summary>
        /// Danh sahs các quốc gia.
        /// </summary>
        /// <returns></returns>
        public static List<SelectListItem> Countries()
        {
            List<SelectListItem> List = new List<SelectListItem>();

            List.Add(new SelectListItem()
            {
                Value = " ",
                Text = "---Chọn Quốc Gia---"

            });

            foreach (var item in CommonDataService.ListOfCountries())
            {
                List.Add(new SelectListItem()
                {
                    Value = item.CountryName,
                    Text = item.CountryName

                });
            }

            return List;
        }

        /// <summary>
        /// Chuyển đổi định dạng ngày tháng
        /// </summary>
        /// <param name="s"></param>
        /// <param name="format"></param>
        /// <returns></returns>
        public static DateTime? DMYStringToDateTime(string s, string format = "dd/MM/yyyy")
        {
            try
            {
                return DateTime.ParseExact(s, format, CultureInfo.InvariantCulture);
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// chuyển đổi cokkie sang tài khoản.
        /// </summary>
        /// <param name="cookie"></param>
        /// <returns></returns>
        public static UserAcount CookieroUserAccount(string cookie)
        {
            return Newtonsoft.Json.JsonConvert.DeserializeObject<UserAcount>(cookie);
        }

        /// <summary>
        /// Chuyển định dạng ngày tháng sang đinh dạng string.
        /// </summary>
        /// <param Datetime="date">Ngày cần chuyển</param>
        /// <returns></returns>
        public static string DMYDateTimeToString(DateTime date)
        {
            try
            {
                return date.ToString("MMM .dd,yyyy");
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// Danh sách loại hàng.
        /// </summary>
        /// <returns></returns>
        public static List<SelectListItem> ListOfCategory()
        {
            List<SelectListItem> List = new List<SelectListItem>();

            List.Add(new SelectListItem()
            {
                Value = "0",
                Text = "---Chọn Loại Hàng---"

            });

            string searchvalue = "";
            foreach (var item in CommonDataService.ListofCategories(searchvalue))
            {
                List.Add(new SelectListItem()
                {
                    Value = item.CategoryID.ToString(),
                    Text = item.CategoryName
                });
            }

            return List;
        }

        /// <summary>
        /// Danh sách nhà cung cấp.
        /// </summary>
        /// <returns></returns>
        public static List<SelectListItem> ListOfSupplier()
        {
            List<SelectListItem> List = new List<SelectListItem>();

            List.Add(new SelectListItem()
            {
                Value = "0",
                Text = "---Chọn Nhà Cung Cấp---"

            });

            string searchvalue = "";
            foreach (var item in CommonDataService.ListOfSuppliers(searchvalue))
            {
                List.Add(new SelectListItem()
                {
                    Value = item.SupplierID.ToString(),
                    Text = item.SupplierName
                });
            }

            return List;
        }

        /// <summary>
        /// Danh sách nhà cung cấp.
        /// </summary>
        /// <returns></returns>
        public static List<SelectListItem> ListOfEmployees()
        {
            List<SelectListItem> List = new List<SelectListItem>();

            List.Add(new SelectListItem()
            {
                Value = "0",
                Text = "---Chọn Nhân viên---"

            });

            string searchvalue = "";
            foreach (var item in CommonDataService.ListOfEmployees(searchvalue))
            {
                List.Add(new SelectListItem()
                {
                    Value = item.EmployeeID.ToString(),
                    Text = item.FirstName + " " + item.LastName
                });
            }

            return List;
        }

        /// <summary>
        /// Danh sách nhà cung cấp.
        /// </summary>
        /// <returns></returns>
        public static List<SelectListItem> ListOfCustomers()
        {
            List<SelectListItem> List = new List<SelectListItem>();

            List.Add(new SelectListItem()
            {
                Value = "0",
                Text = "---Chọn khách hàng---"

            });

            string searchvalue = "";
            foreach (var item in CommonDataService.ListOfCustomers(searchvalue))
            {
                List.Add(new SelectListItem()
                {
                    Value = item.CustomerID.ToString(),
                    Text = item.CustomerName
                });
            }

            return List;
        }


        public static string StatusDescription(int a)
        {
            
                switch (a)
                {
                    case 1:
                        return "Đơn hàng mới. Đang chờ duyệt";
                    case 2:
                        return "Đơn đã chấp nhận. Đang chờ chuyển hàng";
                     case 3:
                        return "Đơn hàng đang được giao";
                    case 4:
                        return "Đơn hàng đã hoàn tất";
                    case -1:
                        return "Đơn hàng đã bị hủy";
                    case -2:
                        return "Đơn hàng bị từ chối";
                    default:
                        return "";
                }
            
        }

        /// <summary>
        /// Danh sách trạng thái đơn hàng.
        /// </summary>
        /// <returns></returns>
        public static List<SelectListItem> Listoffstatus()
        {
            List<SelectListItem> List = new List<SelectListItem>();

            List.Add(new SelectListItem()
            { 
                Value = "0",
                Text = "---Trạng thái---"

            });

            int[] array = new int[] { 1,2,3,4,-1,-2};

            foreach (int status in array)
            {
                List.Add(new SelectListItem()
                {
                    Value = status.ToString(),
                    Text = StatusDescription(status)
                }) ;
            }
           


            return List;
        }
    }
}