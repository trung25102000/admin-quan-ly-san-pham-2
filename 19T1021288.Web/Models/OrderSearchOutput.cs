﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using LiteCommerce.DomainModel;

namespace _19T1021288.Web.Models
{
    public class OrderSearchOutput:PaginationSearchOutput
    {
        public List<Order> data;
    }
}