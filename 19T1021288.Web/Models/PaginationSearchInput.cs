﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace _19T1021288.Web.Models
{
    /// <summary>
    /// Biểu diễn dữ liệu đâu vào để tìm kiếm và phân trang chung.
    /// </summary>
    public class PaginationSearchInput
    {
        /// <summary>
        /// Trang cần hiển thị.
        /// </summary>
        public int Page { get; set; }

        /// <summary>
        /// Số dòng cần hiển thị trên 1 trang.
        /// </summary>
        public int PageSize { get; set; }

        /// <summary>
        /// Giá trị tìm kiếm.
        /// </summary>
        public string SearchValue { get; set; }

    }
    public class ProductSearchInput
    {
        /// <summary>
        /// Trang cần hiển thị.
        /// </summary>
        public int Page { get; set; } 

        /// <summary>
        /// Số dòng cần hiển thị trên 1 trang.
        /// </summary>
        public int PageSize { get; set; } 

        /// <summary>
        /// Giá trị tìm kiếm.
        /// </summary>
        public string SearchValue { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int CategoryID { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int SupplierID { get; set; }
    }

    public class OrderSearchInput:PaginationSearchInput
    {
        /// <summary>
        /// mã trạng thái
        /// </summary>
        public int status;
    }
}