﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using _19T1021288.DomainModels;
using LiteCommerce.DomainModel;

namespace _19T1021288.Web.Models
{
    /// <summary>
    /// 
    /// </summary>
    public class ProductSearchOutput:PaginationSearchOutput
    {
        public List<Product> data { get; set; }
    }
}