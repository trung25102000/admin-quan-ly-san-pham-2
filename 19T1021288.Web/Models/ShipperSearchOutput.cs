﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using _19T1021288.DomainModels;

namespace _19T1021288.Web.Models
{

    /// <summary>
    /// Kết quả tìm kiếm người giao hàng dưới dạng phân trang.
    /// </summary>
    public class ShipperSearchOutput:PaginationSearchOutput
    {
        /// <summary>
        /// Danh sách người giao hàng.
        /// </summary>
        public List<Shipper> data { get; set; }
    }
}