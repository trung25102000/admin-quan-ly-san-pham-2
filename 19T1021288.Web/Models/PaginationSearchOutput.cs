﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace _19T1021288.Web.Models
{
    /// <summary>
    /// Lớp cơ sở cho các lớp dùng để lưu trữ kết quả tìm kiếm dưới dạng phân trang.
    /// </summary>
    public abstract class PaginationSearchOutput
    {
        /// <summary>
        /// Trang cần hiển thị.
        /// </summary>
        public int Page { get; set; }

        /// <summary>
        /// Số dòng mỗi trang.
        /// </summary>
        public int PageSize { get; set; }

        /// <summary>
        /// Giá trị tìm kiếm
        /// </summary>
        public string SearchValue { get; set; }

        /// <summary>
        /// Số kết quả tìm được.
        /// </summary>
        public int RowCount { get; set; }

        /// <summary>
        /// Tổng số trang.
        /// </summary>
        public int PageCount
        {
            get
            {
                if (PageSize == 0) return 1;
                int p = RowCount / PageSize;
                if (RowCount % PageSize > 0) p =p +1;
                return p;
            }
        }


    }
}