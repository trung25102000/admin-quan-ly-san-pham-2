﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using _19T1021288.DomainModels;

namespace _19T1021288.Web.Models
{
    public class employeeSearchOutput :PaginationSearchOutput
    {
        public List<Employee> data { get; set; }
    }
}