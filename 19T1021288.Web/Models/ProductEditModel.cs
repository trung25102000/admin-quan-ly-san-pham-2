﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using LiteCommerce.DomainModel;

namespace _19T1021288.Web.Models
{
    public class ProductEditModel
    {
        public Product Data { get; set; }

        public List<ProductAttribute> ListProductAttribute { get; set; }

        public List<ProductPhoto> ListProductPhoto { get; set; }

    }
}