﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _19T1021288.DataLeyers
{
    /// <summary>
    /// Định nghĩa các phép dữ liệu chung cho các dữ liệu
    /// đơn giản tương tự trên các bảng
    /// </summary>
    public interface ICommonDAL<T> where T: class
    {
        /// <summary>
        /// tìm kiếm và lấy danh sách dữ liệu dưới dạng có phân trang
        /// </summary>
        /// <param name="page">trang dữ liệu cần hiển thị</param>
        /// <param name="pageSize">Số dòng được hiển thị trên mỗi trang(0 tức là Không yêu cầu phân trang)</param>
        /// <param name="searcgValue">Tên cần tìm kiếm (chuỗi rống nêu không tìm kiếm)</param>
        /// <returns></returns>
        IList<T> List(int page = 1, int pageSize = 0, string searcgValue = "");

        /// <summary>
        /// đếm số dòng dữ liệu tìm được 
        /// </summary>
        /// <param name="searchValue">Tên cần tìm kiếm (chuỗi rống nêu không tìm kiếm )</param>
        /// <returns></returns>
        int Count(string searchValue = "");

        /// <summary>
        /// Lấy thông tin của dữ liệu dựa vào ID
        /// </summary>
        /// <param name="ID">ID của dữ liệu</param>
        /// <returns></returns>
        T Get(int ID);

        /// <summary>
        /// Bổ sung một dữ liệu mới vào cơ sở dữ liệu
        /// </summary>
        /// <param name="data"></param>
        /// <returns>ID của dữ liệu tạo mới </returns>
        int Add(T data);

        /// <summary>
        /// cập nhập thông tin của 1 dữ liệu chỉ định
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        bool Update(T data);

        /// <summary>
        /// Xóa dữ liêu dựa vào ID
        /// </summary>
        /// <param name="ID"> ID của dữ liệu</param>
        /// <returns></returns>
        bool Delete(int ID);

        /// <summary>
        ///kiểm tra dữ liệu hiện có dữ liệu liên quan không
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        bool InUsed(int ID);

        /// <summary>
        /// Đếm số dữ liệu được dùng.
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        int Count(int ID);

    }
}
