﻿using _19T1021288.DomainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _19T1021288.DataLeyers
{
    /// <summary>
    /// định nghĩa các Xử lý các dữ liệu nhà cung cấp
    /// (Sử dụng cách này dẫn đến viết lặp đi lặp lại các kiểu code giống nhau 
    /// cho các đối tượng dữ liệu tương tự như khách hàng,nhân viên,...) =>không dùng.
    /// => dùng cách viết generic
    /// </summary>
    public interface ISupplierDAL
    {
        /// <summary>
        /// tìm kiếm và lấy danh sách các nhà cung cấp dưới dạng có phân trang
        /// </summary>
        /// <param name="page">trang dữ liệu cần hiển thị</param>
        /// <param name="pageSize">Số dòng được hiển thị trên mỗi trang(0 tức là Không yêu cầu phân trang)</param>
        /// <param name="searcgValue">Tên cần tìm kiếm (chuỗi rống nêu không tìm kiếm theo tên)</param>
        /// <returns></returns>
        IList<Supplier> List(int page = 1, int pageSize = 0, string searcgValue = "");

        /// <summary>
        /// đếm số nhà cung cấp tìm được 
        /// </summary>
        /// <param name="searchValue">Tên cần tìm kiếm (chuỗi rống nêu không tìm kiếm theo tên)</param>
        /// <returns></returns>
        int Count(string searchValue = "");

        /// <summary>
        /// Lấy thông tin của một nhà cung cấp dựa vào nhà cung cấp
        /// </summary>
        /// <param name="SupplierID">Mã của nhà cung cấp cần lấy</param>
        /// <returns></returns>
        Supplier Get(int SupplierID);

        /// <summary>
        /// Bổ sung một nhà cung cấp mới vào cơ sở dữ liệu
        /// </summary>
        /// <param name="data"></param>
        /// <returns>ID của nhà cung cấp được tạo mới </returns>
        int Add(Supplier data);

        /// <summary>
        /// cập nhập thông tin của nhà cung cấp
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        bool Update(Supplier data);

        /// <summary>
        /// Xóa nhà cung cấp dựa vào mã của nhà cung cấp
        /// </summary>
        /// <param name="SupplierID"> Mã nhà cung cấp cần xóa</param>
        /// <returns></returns>
        bool Delete(int SupplierID);

        /// <summary>
        ///kiểm tra Nhà cung cấp hiện có dữ liệu liên quan không
        /// </summary>
        /// <param name="SupplierID"></param>
        /// <returns></returns>
        bool InUsed(int SupplierID);


    }
}
