﻿using _19T1021288.DomainModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _19T1021288.DataLeyers.SQLServer
{
    public class CustomerAccountDAL : _BaseDAL, IUserAccountDAL
    {
        /// <summary>
        /// Hàm tạo để connec CSDL.
        /// </summary>
        /// <param name="ConnectionString"></param>
        public CustomerAccountDAL(string ConnectionString) : base(ConnectionString)
        {

        }

        /// <summary>
        /// Kiểm tra tài khoản của khach hàng
        /// </summary>
        /// <param name="UserName"></param>
        /// <param name="Passwword"></param>
        /// <returns></returns>
        UserAcount IUserAccountDAL.Authorize(string UserName, string Passwword)
        {
            UserAcount data = null;

            using (SqlConnection cn = OpenConnection())
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = @"select CustomerID,CustomerName,Email
                                        from Customers 
                                        where Email=@Email and Password=@Password";
                cmd.CommandType = CommandType.Text;
                cmd.Connection = cn;
                cmd.Parameters.AddWithValue("@Email", UserName);
                cmd.Parameters.AddWithValue("@Password", Passwword);

                var dbReader = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                if (dbReader.Read())
                {
                    data = new UserAcount()
                    {
                        UserID = Convert.ToString(dbReader["CustomerID"]),
                        UserName=Convert.ToString(dbReader["CustomerName"]),
                        Email=Convert.ToString(dbReader["Email"]),
                        Password=Convert.ToString(dbReader["Password"])
                    };
                }

                cn.Close();
            }

            return data;
        }

        /// <summary>
        /// Cập nhật mật khẩu cho khách hàng.
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="OldPassword"></param>
        /// <param name="NewPassword"></param>
        /// <returns></returns>
        bool IUserAccountDAL.ChangePasswword(string userName, string OldPassword, string NewPassword)
        {
            bool result = false;

            using (SqlConnection cn = OpenConnection())
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = @"update customers
                                        set Password=@NewPassword
                                        where Email=@Email and Password=@OldPassword";
                cmd.CommandType = CommandType.Text;
                cmd.Connection = cn;
                cmd.Parameters.AddWithValue("@Email", userName);
                cmd.Parameters.AddWithValue("@NewPassword", NewPassword);
                cmd.Parameters.AddWithValue("@OldPassword", OldPassword);

                result = cmd.ExecuteNonQuery() > 0;

                cn.Close();
            }

            return result;
        }
    }
}
