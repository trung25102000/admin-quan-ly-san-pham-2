﻿using _19T1021288.DomainModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _19T1021288.DataLeyers.SQLServer
{
    /// <summary>
    /// Xử lý dữ liệu liên quan đến tài khoản của nhân viên.
    /// </summary>
    public class EmployeeAccountDAL : _BaseDAL, IUserAccountDAL
    {
        /// <summary>
        /// Hàm tạo đế cconnect CSDL.
        /// </summary>
        /// <param name="connectionString"></param>
        public EmployeeAccountDAL(string connectionString):base(connectionString)
        {

        }

        /// <summary>
        /// Kiểm tra tài khoản đăng nhập của nhân viên có hợp lệ hay ko?
        /// </summary>
        /// <param name="UserName">Email của nhân viên.</param>
        /// <param name="Passwword">Mật khẩu củ nhân viên.</param>
        /// <returns>Trả  về UserAcount của nhân viên đó nếu hợp lệ và null nếu ngc lại.</returns>
        UserAcount IUserAccountDAL.Authorize(string UserName, string Passwword)
        {
            UserAcount data = null;

            using (SqlConnection cn = OpenConnection())
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = @"select EmployeeID,FirstName,LastName,Email,Photo,BirthDate
                                        from Employees 
                                        where Email=@Email and Password=@Password";
                cmd.CommandType = CommandType.Text;
                cmd.Connection = cn;
                cmd.Parameters.AddWithValue("@Email", UserName);
                cmd.Parameters.AddWithValue("@Password", Passwword);

                var dbReader = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                if (dbReader.Read())
                {
                    data = new UserAcount()
                    {
                        UserID = Convert.ToString(dbReader["EmployeeID"]),
                        UserName = Convert.ToString(dbReader["Email"]),
                        FullName=$"{dbReader["Firstname"]} {dbReader["LastName"]}",
                        Email= Convert.ToString(dbReader["Email"]),
                        Images=Convert.ToString(dbReader["Photo"]),
                        Birthdate=Convert.ToDateTime(dbReader["BirthDate"]),
                        Password="",
                        RoleNames=""
                    };
                }

                cn.Close();
            }
            return data;

        }

        /// <summary>
        /// Đổi mật khẩu cho nhân viên.
        /// </summary>
        /// <param name="userName">Email của nhân viên.</param>
        /// <param name="OldPassword">Mật khẩu cũ</param>
        /// <param name="NewPassword">Mật khẩu mới muốn đổi.</param>
        /// <returns>Truê:nếu có sự thay đổi và false ngc lại</returns>
        bool IUserAccountDAL.ChangePasswword(string userName, string OldPassword, string NewPassword)
        {
            bool result = false;

            using (SqlConnection cn = OpenConnection())
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = @"update Employees
                                        set Password=@NewPassword
                                        where Email=@Email and Password=@OldPassword";
                cmd.CommandType = CommandType.Text;
                cmd.Connection = cn;
                cmd.Parameters.AddWithValue("@Email", userName);
                cmd.Parameters.AddWithValue("@NewPassword", NewPassword);
                cmd.Parameters.AddWithValue("@OldPassword", OldPassword);

                result = cmd.ExecuteNonQuery() > 0;

                cn.Close();
            }

            return result;
        }
    }
}
