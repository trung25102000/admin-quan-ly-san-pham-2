﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using _19T1021288.DomainModels;

namespace _19T1021288.DataLeyers.SQLServer
{
    /// <summary>
    /// Cài đặt các phép xử lý dữ liệu cho loại hàng
    /// </summary>
    public class CategoryDAL : _BaseDAL, ICommonDAL<Category>
    {
        /// <summary>
        /// Custor
        /// </summary>
        /// <param name="connectionString"></param>
        public CategoryDAL(String connectionString) : base(connectionString)
        {

        }

        /// <summary>
        /// Thêm 1 loại hàng vào dữ liệu
        /// </summary>
        /// <param name="data">1 loại hàng</param>
        /// <returns>Mã loại hàng mới thêm,hoặc trả về 0 nếu không thêm.</returns>
        int ICommonDAL<Category>.Add(Category data)
        {
            int result = 0;
            using (SqlConnection cn = OpenConnection())
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = @"INSERT INTO categories(CategoryName,Description,ParentCategoryId)
                                    VALUES(@CategoryName,@Description,@ParentCategoryId);
                                    SELECT SCOPE_IDENTITY()";
                cmd.CommandType = CommandType.Text;
                cmd.Connection = cn;
                cmd.Parameters.AddWithValue("@CategoryName", data.CategoryName);
                cmd.Parameters.AddWithValue("@Description", data.Description);
                cmd.Parameters.AddWithValue("@ParentCategoryId", data.ParentCategoryId);

                result = Convert.ToInt32(cmd.ExecuteScalar());

                cn.Close();
            }
            return result;
        }

        /// <summary>
        /// Đếm số loại hàng tìm được theo tìm kiếm tương đối.
        /// </summary>
        /// <param name="searchValue">Từ khóa để tìm kiếm</param>
        /// <returns>Số hàng tương ứng với từ khóa.</returns>
        int ICommonDAL<Category>.Count(string searchValue = " ")
        {
            int count = 0;

            if (searchValue != "")
                searchValue = "%" + searchValue + "%";

            using (SqlConnection cn = OpenConnection())
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = @"SELECT	COUNT(*)
                                    FROM	Categories 
                                    WHERE	(@SearchValue = N'')
	                                    OR	(
			                                    (CategoryName LIKE @SearchValue)
			                                    OR (Description LIKE @SearchValue)
		                                    )";
                cmd.CommandType = CommandType.Text;
                cmd.Connection = cn;
                cmd.Parameters.AddWithValue("@SearchValue", searchValue);

                count = Convert.ToInt32(cmd.ExecuteScalar());

                cn.Close();
            }
            return count;
        }

        /// <summary>
        /// XÓa 1 loại hàng dựa trên mã hàng.
        /// </summary>
        /// <param name="ID">Mã loại hàng</param>
        /// <returns>(T: nếu xóa thành công,F: nếu thất bại).</returns>
        bool ICommonDAL<Category>.Delete(int ID)
        {
            bool result = false;
            using (SqlConnection cn = OpenConnection())
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = @"DELETE FROM Categories 
                                    WHERE CategoryID = @CategoryID AND NOT EXISTS(SELECT * FROM Products WHERE CategoryID = @CategoryID)";
                cmd.CommandType = CommandType.Text;
                cmd.Connection = cn;
                cmd.Parameters.AddWithValue("@CategoryID", ID);

                result = cmd.ExecuteNonQuery() > 0;

                cn.Close();
            }
            return result;
        }

        /// <summary>
        /// Lấy thông tin 1 loại hàng dựa trên mã loại hàng.
        /// </summary>
        /// <param name="ID">Mã Loại Hàng</param>
        /// <returns>Thông tin loại hàng đó.Trả về NULL nếu ko tìm thấy.</returns>
        Category ICommonDAL<Category>.Get(int ID)
        {
            Category data = null;
            using (SqlConnection cn = OpenConnection())
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = @"SELECT * FROM Categories WHERE CategoryID = @CategoryID";
                cmd.CommandType = CommandType.Text;
                cmd.Connection = cn;
                cmd.Parameters.AddWithValue("@CategoryID", ID);
                var dbReader = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                if (dbReader.Read())
                {
                    data = new Category()
                    {
                        CategoryID = Convert.ToInt32(dbReader["CategoryID"]),
                        CategoryName = Convert.ToString(dbReader["CategoryName"]),
                        Description = Convert.ToString(dbReader["Description"]),
                        ParentCategoryId = Convert.ToInt32(dbReader["ParentCategoryId"])
                    };
                }
                cn.Close();
            }
            return data;
        }

        /// <summary>
        /// Kiểm tra xem loại hàng có đã tòn tại ở bảng hàng hay chưa?
        /// </summary>
        /// <param name="ID">Mã loại hàng</param>
        /// <returns>(T: nếu có,F: nếu ko)</returns>
        bool ICommonDAL<Category>.InUsed(int ID)
        {
            bool result = false;
            using (SqlConnection cn = OpenConnection())
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = @"SELECT CASE 
                                                WHEN EXISTS(SELECT * FROM Products WHERE CategoryID = @CategoryID) THEN 1 
                                                ELSE 0 
                                            END";
                cmd.CommandType = CommandType.Text;
                cmd.Connection = cn;
                cmd.Parameters.AddWithValue("@CategoryID", ID);

                result = Convert.ToBoolean(cmd.ExecuteScalar());

                cn.Close();
            }
            return result;
        }

        /// <summary>
        /// Tìm kiếm và lấy danh sách loại hàng dưới dạng phân trang.
        /// </summary>
        /// <param name="page">Trang cần xem</param>
        /// <param name="pageSize">Số dòng trên mỗi trang</param>
        /// <param name="searcgValue">Giá trị tìm kiếm</param>
        /// <returns>Trả về danh sách dưới dạng phân trang</returns>
        IList<Category> ICommonDAL<Category>.List(int page, int pageSize, string searchValue)
        {
            List<Category> data = new List<Category>();

            if (searchValue != "")
                searchValue = "%" + searchValue + "%";

            using (SqlConnection cn = OpenConnection())
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = @"SELECT *
                                    FROM 
                                    (
	                                    SELECT	*, ROW_NUMBER() OVER (ORDER BY CategoryName) AS RowNumber
	                                    FROM	Categories 
	                                    WHERE	(@SearchValue = N'')
		                                    OR	(
				                                    (CategoryName LIKE @SearchValue)
			                                     OR (Description LIKE @SearchValue)
			                                    )
                                    ) AS t
                                    WHERE (@PageSize = 0) OR (t.RowNumber BETWEEN (@Page - 1) * @PageSize + 1 AND @Page * @PageSize);";
                cmd.CommandType = CommandType.Text;
                cmd.Connection = cn;

                cmd.Parameters.AddWithValue("@Page", page);
                cmd.Parameters.AddWithValue("@PageSize", pageSize);
                cmd.Parameters.AddWithValue("@SearchValue", searchValue);

                var dbReader = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                while (dbReader.Read())
                {
                    data.Add(new Category()
                    {
                        CategoryID = Convert.ToInt32(dbReader["CategoryID"]),
                        CategoryName = Convert.ToString(dbReader["CategoryName"]),
                        Description = Convert.ToString(dbReader["Description"]),
                        ParentCategoryId = Convert.ToInt32(dbReader["ParentCategoryId"])
                    });
                }
                dbReader.Close();
                cn.Close();
            }

            return data;
        }

        /// <summary>
        /// Cập nhật lại thông tin loại hàng
        /// </summary>
        /// <param name="data">Dữ liệu hàng có thông tin thay đổi</param>
        /// <returns>(T: nếu có capaj nhât,F: nếu không)</returns>
        bool ICommonDAL<Category>.Update(Category data)
        {
            bool result = false;
            using (SqlConnection cn = OpenConnection())
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = @"UPDATE Categories
                                    SET CategoryName = @CategoryName, Description = @Description, ParentCategoryId = @ParentCategoryId
                                    WHERE CategoryID = @CategoryID";
                cmd.CommandType = CommandType.Text;
                cmd.Connection = cn;
                cmd.Parameters.AddWithValue("@CategoryID", data.CategoryID);
                cmd.Parameters.AddWithValue("@CategoryName", data.CategoryName);
                cmd.Parameters.AddWithValue("@Description", data.Description);
                cmd.Parameters.AddWithValue("@ParentCategoryId", data.ParentCategoryId);


                result = cmd.ExecuteNonQuery() > 0;

                cn.Close();
            }
            return result;
 
        }

        /// <summary>
        /// Đếm số lượng sản phẩm có loại hàng với mã loại hàng đưa vào.
        /// </summary>
        /// <param name="ID">Mã loại hàng đưa vào.</param>
        /// <returns>Số sản phẩm</returns>
        int ICommonDAL<Category>.Count(int ID)
        {
            int count = 0;
            using(SqlConnection cn=OpenConnection())
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = @"SELECT	COUNT(*)
                                    FROM	products 
                                    WHERE	CategoryID=@ID ";
                cmd.CommandType = CommandType.Text;
                cmd.Connection = cn;
                cmd.Parameters.AddWithValue("@ID", ID);

                count = Convert.ToInt32(cmd.ExecuteScalar());

                cn.Close();
            }    
            return count;
        }

       
    }
}

