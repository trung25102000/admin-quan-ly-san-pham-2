﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using _19T1021288.DomainModels;

namespace _19T1021288.DataLeyers.SQLServer
{
    /// <summary>
    /// 
    /// </summary>
    public class CountryDAL: _BaseDAL,ICountryDAL
    {

        /// <summary>
        /// Custor
        /// </summary>
        /// <param name="connectionString"></param>
        public CountryDAL(String connectionString) :base(connectionString)
        {

        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IList<Country> List()
        {
            List<Country> data = new List<Country>();
            using (var connection = OpenConnection())
            {
                SqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = "Select CountryName from Countries";
                cmd.CommandType = CommandType.Text;

                SqlDataReader dbReader = cmd.ExecuteReader(CommandBehavior.CloseConnection);

                while(dbReader.Read())
                {
                    data.Add(new Country()
                    {
                        CountryName = Convert.ToString(dbReader["CountryName"])
                    }) ;
                }    

                dbReader.Close();
                connection.Close();
            }    

            return data;
        }
    }
}
