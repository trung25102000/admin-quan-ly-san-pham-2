﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using _19T1021288.DomainModels;

namespace _19T1021288.DataLeyers
{
    /// <summary>
    /// Định nghĩa các phép xử lý liên quan đến dữ liệu tài khoản ng dùng.
    /// </summary>
    public interface IUserAccountDAL
    {
        /// <summary>
        /// Kiểm tra tên đăng nhập và mật khẩu có hợp lệ hay ko?
        /// </summary>
        /// <param name="UserName">Tên tài khoản</param>
        /// <param name="Passwword">Mật khẩu</param>
        /// <returns>Nếu hợp lệ thì trả về thông tin tài khoản, ngươc lại trae về null</returns>
        UserAcount Authorize(string UserName, string Passwword);

        /// <summary>
        /// Đổi mật khẩu.
        /// </summary>
        /// <param name="userName">Tên tài khoản cần đổi.</param>
        /// <param name="OldPassword">Mật khẩu cũ.</param>
        /// <param name="NewPassword">Mật khẩu mới.</param>
        /// <returns>True: nếu đổi thành công và ngược lại thì false.</returns>
        bool ChangePasswword(string userName, string OldPassword, string NewPassword);
    }
}
